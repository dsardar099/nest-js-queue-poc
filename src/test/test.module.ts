import { Module } from '@nestjs/common';
import { IntegrationModule } from 'src/integration/integration.module';
import { TestController } from './test.controller';
import { TestService } from './test.service';

@Module({
  imports: [IntegrationModule],
  controllers: [TestController],
  providers: [TestService],
})
export class TestModule {}
