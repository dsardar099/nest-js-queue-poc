import { InjectQueue } from '@nestjs/bull';
import { Injectable } from '@nestjs/common';
import { Queue } from 'bull';

@Injectable()
export class TestService {
  constructor(
    @InjectQueue('integration') private readonly integrationQueue: Queue,
  ) {}

  async getTest() {
    for (let i = 0; i < 15; i++) {
      console.log('Job not added to queue-' + i);
      const payload = {
        type: i % 2 === 0 ? 'two' : i % 3 === 0 ? 'three' : 'one',
        data: {
          id: 'x1x1' + i,
          name: 'YY ZZ' + i,
        },
      };
      console.log('Payload: ' + JSON.stringify(payload));
      const cr = await this.integrationQueue.add(
        'handle-integrtion-task',
        payload,
      );
      console.log('Job added to queue-' + i, '=', cr);
      console.log('Added job to queue-' + i);
    }
    console.log('Returning...');
    return { success: true, message: 'Added jobs to queue' };
  }
}
