import { Controller, Get } from '@nestjs/common';
import { IntegrationService } from './integration.service';

@Controller('integration')
export class IntegrationController {
  constructor(private readonly integrationService: IntegrationService) {}

  @Get('/one')
  getOne() {
    return this.integrationService.fetchAPIOne();
  }

  @Get('/two')
  getTwo() {
    return this.integrationService.fetchAPITwo();
  }

  @Get('/three')
  getThree() {
    return this.integrationService.fetchAPIThree();
  }
}
