import { BullModule } from '@nestjs/bull';
import { Module } from '@nestjs/common';
import { join } from 'path';
import { IntegrationController } from './integration.controller';
import { IntegrationProcessor } from './integration.processor';
import { IntegrationService } from './integration.service';

const queue = BullModule.registerQueue({
  name: 'integration',
  // processors: [join(__dirname, './integration.processor')],
});

@Module({
  imports: [queue],
  controllers: [IntegrationController],
  providers: [IntegrationService, IntegrationProcessor],
  exports: [IntegrationService, IntegrationProcessor, queue],
})
export class IntegrationModule {}
