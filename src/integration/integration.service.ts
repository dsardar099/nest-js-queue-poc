import { Injectable } from '@nestjs/common';
import axios from 'axios';

@Injectable()
export class IntegrationService {
  async fetchAPIOne() {
    console.log('fetchAPIOne start');
    const resp = await axios.get(
      'https://jsonplaceholder.typicode.com/comments',
    );
    console.log('fetchAPIOne stop:');
    return resp.data;
  }

  async fetchAPITwo() {
    console.log('fetchAPITwo start');
    const resp = await axios.get('https://jsonplaceholder.typicode.com/posts');
    console.log('fetchAPITwo stop');
    return resp.data;
  }

  async fetchAPIThree() {
    console.log('fetchAPIThree start');
    const resp = await axios.get('https://jsonplaceholder.typicode.com/todos');
    console.log('fetchAPIThree stop');
    return resp.data;
  }
}
