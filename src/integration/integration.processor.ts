import { Process, Processor } from '@nestjs/bull';
import { Logger } from '@nestjs/common';
import { Job } from 'bull';
import { IntegrationService } from './integration.service';

@Processor('integration')
export class IntegrationProcessor {
  private readonly logger = new Logger(IntegrationProcessor.name);
  constructor(private readonly integrationService: IntegrationService) {}

  @Process('handle-integrtion-task')
  async handleIntegrationTask(job: Job) {
    console.log('Start handle-integrtion-task...');
    console.log(job.data);
    if (job.data.type === 'one') {
      console.log('Processing API One...');
      await this.integrationService.fetchAPIOne();
    } else if (job.data.type === 'two') {
      console.log('Processing API Two...');
      await this.integrationService.fetchAPITwo();
    } else if (job.data.type === 'three') {
      console.log('Processing API Three...');
      await this.integrationService.fetchAPIThree();
    }
    console.log('handle-integrtion-task completed');
  }
}
